const NOVEL_API = 'ulysses.json';

export const addNovel = payload => ({
  type: 'FETCH_NOVEL',
  payload,
});

export const clearNovel = () => ({ type: 'CLEAR_NOVEL' });

export const fetchNovel = () => async dispatch => {
  try {
    const response = await fetch(NOVEL_API)
    const responseBody = await response.json();
    dispatch(addNovel(responseBody));
  } catch (error) {
    console.error(error);
    dispatch(clearNovel());
  }
}
