# Introduction

This document isn't meant to be a dead document, religious text or manifesto.  It's meant to gently guide developers on our team toward better practices and to accelerate dissemination of lessons learned.  Most importantly this aims to provoke discussion and welcome contributions from all with valid points and great ideas.

A lot of these ideas cut both ways -- to curtail discussion each section will end with trying to acknowledge the pros and cons of the suggestion.

Developer's Note: There's many example applications in this repository that give the reader a chance to play around with code and see illustrations of what each section is talking about.  To run this applications, change directory into the given example project and run `npm install; npm start`.

# ES6

The following sections aim to cover a few things that are not covered by our linter and could maybe be construed as coder's choice without some guidance.

## Naming

Variables inside closures should have camel case like `const availableQuantityInStore = 1;` the same goes for functions, class functions, helper functions, any functions without serious side effects that aren't components.  Variables in constants files used across components and files should (at the top level) have all caps like `export const MAXIMUM_HEAD_ROOM = '7 feet'` and they shouldn't be too complicated.  Classes like components and even functional components should have capitalized names like `class StandardChessBoard extends React.Component`.  Files that contain such components should be identically named.  Files that contain helper methods or utility functions or just represent extracted logic in general should be called something helpful to future developers and should be camelcased.  For example, if I have a file that manages the functions for determining if the king is in check or checkmate I might make a file in the chess application called `mateStatus.js` inside the container directory for the board.  

Acronyms: TBD.

_pros_: Consistency across the codebase makes code more readable.

_cons_: Getting used to the convention and outliers can be visually unappealing.

## Accessing Properties & Default values

There may be some debate about whether or not code should fail and fail hard when there's something you expected as a basic argument.  But on our team serving pages as we do, I have been hard pressed to find a good example where we wanted execution to stop and stop hard.  The best scenario is that a section just doesn't render but the rest of the application behaves like adults wrote it.  

So let's say you have a really deep object `foo` that has a deep shape and we want to access a leaf property like so:

```javascript
let foo = { bar: { cue: { zoo: [ 1, 2, 3 ] } } };
const zooFirstValue = foo.bar.cue.zoo[0];
```

Looks pretty straight forward right?  Every thing is okay -- but what if `foo` is being passed into some function or coming from a service call?  It may even seem like this is unlikely and you may search the code for it but if your team engages in reuse, someone may unknowingly pass in a more risky variable.  

In our codebase you may come across things like `lodash`'s `get` in use.  And they look like this:

```javascript
import { get } from 'lodash';
const zooFirstValue = get(foo, 'bar.cue.zoo[0]', 0);
```

Where the first parameter is the primary top level object and the following parameter is the string of accessing (it can even do indices of arrays as shown).  The last parameter is the default value if that accessing string fails or finds nothing.

What we've moved to is equivalent but slightly more performant and now preferred on our team so if you are writing new code you must use this and if you're a good little developer and want a candy you will take the time to replace the `get` usages with this `idx`.  Replicating the code above:

```javascript
import idx from 'idx';
const zooFirstValue = idx(foo, _ => _.bar.cue.zoo[0]) || 0;
```

The first paramenter is the same but the second parameter is an arrow function whereby we prefer the underscore character as the input variable.  Notice that if you want a default value you must use a logical or so that if this expression evaluates to `null` or `undefined`, we fail over to the default.  

There's a different kind of safety that can be handled with ES6's destructor syntax to safely handle accessing properties.  Let's look at the following two code snippets:

```javascript
const getValue = tuple => {
  if(tuple.valid) {
    return tuple.value;
  }
  return `Invalid record but ${tuple.value} was present.`;
}
```

```javascript
const dateString = `the date of purchase is ${this.props.date.month} by ${this.props.buyerName}`;
```

In the first code example we have some interesting codesplosions if `tuple` is `undefined` or `null`.  We prefer to proactively handle this by first making tuple by default an empty object and second we can also make the code a little easier to read by deconstructing it:

```javascript
const getValue = (tuple = {}) => {
  const { valid, value } = tuple;
  if(valid) {
    return value;
  }
  return `Invalid record but ${value} was present.`;
}
```

In the second example, we may know that since we're in a React component that `this.props` will always be an object so we can rely on destructuring to handle any problems:

```javascript
const { date = {}, buyerName } = this.props;
const dateString = `the date of purchase is ${date.month} by ${buyerName}`;
```

There are more advanced destructuring techniques (for example can you destructure month inside the destructure block above?) but for our code standards we're only concerned that things aren't blowing up.  

:exclamation: If you are setting default values in Selectors, please read the section Redux/Reselect to avoid the perils of `{} !== {}` and `[] !== []`.

_pros_: We use these strategies to avoid code blowing up.

_cons_: There is slightly more code to maintain and there are a few pitfalls to watch out for.  

## Iteration

TBD: discussion of `forEach`, `map` & `reduce`.

# ReactJS

It's very easy to get up and running with ReactJS.  Serving tens of thousands of pages to millions of users?  Not so easy.  With any framework or library.  So let's rap about some of that nar nar before your code gets iced ... or maybe "ice" is good?  I don't know, the 80s left me confused and homeless.  

## Component or PureComponent?  

What the actual functional is going on with the different ways to write a component?  Generally you should be writing your components as functional components if you can.  There are outlying cases where this isn't the case but they're rare. A functional component can't be connected and can't have state and, well, you can't override life cycle methods because you're simply not extending a class.  But the nice thing about them is that they (should) have no side effects, are super easy to test and are super easy to reuse.  They are generally just props injected into JSX and look like this:

```javascript
const ScoreCard = (props) => {
  const name = { props };
  const isFriend = { props };
  return <h1>{name} is {isFriend && 'not'} my friend</h1>;
}
```

Aside from the JSX, this looks like a pretty plain ES6 format closure -- and it basically is.  But that leaves two other ways of declaring classes: `React.Component` and `React.PureComponent`.  The basic difference between the two is that the default implementations of props and state compare vary between the two.  `PureComponent` only does a shallow compare of its state and props before deciding if it should or shouldn't update.  So if your props and state are not complex nested structures and your component can benefit from speedups, `PureComponent` may help you.  Generally the differences between the two is so small that it doesn't arise until the performance tuning phase of your app and it's fairly easy to flip between the two.  Just be mindful that flipping to `PureComponent` can protect your code from [needless rerenders](https://gitlab.com/eldavojohn/reactjs-redux-lessons-learned#needless-rerenders-1).

## Needless Rerenders

A commonly overlooked rule for handling ReactJS components is how frequently our components are [needlessly rendered](https://gitlab.com/eldavojohn/reactjs-redux-lessons-learned#needless-rerenders-1).  In a very simple ReactJS application, it's pretty easy to see what's going on and even count the number of times a given component should re-render.  In our time working with these technologies, the bulk of needless rerenders have one source: the asynchronous nature of our thunks being dispatched by Redux actions.  Don't misunderstand this, these asynchronous dispatches to servers are what fundamentally drive our site.  And the ability to dispatch them asynchronously leads to the best possible guest experience.  

To identify rendering issues, take alook at how [the reactjs docs recommend viewing performance in chrome](https://reactjs.org/docs/optimizing-performance.html#profiling-components-with-the-chrome-performance-tab) -- it's a great resource for trying to see if performance issues are related to rerenders.  The simplist issue with needless rerenders is calling `this.setState` at an inappropriate or dangerous time.  Generally our linter will catch these mistakes but it's possible to disable these rules to overcome particularly challenging solutions.  Avoid calling `this.setState` directly inside the lifecycle functions of the component.  Doing so without proper safety nets can cause endless renders.  If you call `this.setState` inside `render()`, you could update state that would cause another rerender ... that would update state that would ... surely you can see how disasterous this would be.  So if you call them inside lifecycle methods, try to be restrictive as possible when calling `setState` or, if you can work that out of your design, you'll leave yourself and others with less responsibility when editing this component in the future.  

If you've identified needless rerenders that aren't simple, the name of the game shouldn't be extinguishing network calls (although if you know our service engineers, please influence them appropriately) but instead to wrap our minds around how we can mitigate rerendering issues.  The first and most important thing to understand is that ReactJS will only invoke the `render()` method in its lifecycle when something doesn't match in `this.props` or `this.state`.  When a connected component is notified of a change in the redux state that it's bound to, there's a function called `mapStateToProps` that returns properties mapped to values.  It's these values -- whether it be directly shown or through reselect -- that sometimes contain problems for rerendering.  On the most basic level these values should be as simple as possible and ideally be the most basic datatypes like string, number, etc.  The reason for this is that a default value of `{}` for an object value presents some hidden issues.  Every time that slice of state is modified by any part of your code or anytime that component mutates ever so slightly, it re-evaluates `mapStateToProps` and finds out that `{} !== {}` which is bad news.  That's going to cause many needless rerenders as will `[] !== []` if you're returning an empty array by default.  It's much better to use `defaultProps` or initialize during deconstruction from `this.props` in the component.  

A tip if you're dealing with a component that has one blob object and a bunch of more atomic properties, try commenting out the big blob.  Another tip for identifying needless rerenders is our team's performance query string that brings up a neat display board at the bottom of the page to let you know some basic details about the performance of a given component.  TBD Bryce.

# Redux

Redux presents a new way to handle state across components.   But there's no free lunch so first we'll cover the more simple methods for state management inside reactjs components -- namely state and callback handlers inside the components.  First there are a handful of situations where we actually don't want to use redux.  If you're not making asynchronous calls in your application and your application is quite simple, you probably don't need advanced state management like redux.  Even if you did have an asynchronous call (but only one) it might not make sense to deal with all the boilerplate of redux.  It's not a silver bullet.

The other time you don't want to use redux is when you're dealing with sensitive data that you want to make as transient as possible.  Credit card numbers, sensitive user details, session identifiers, PINs, keys, etc are all good examples of data that you don't want to store in redux and should be relegated to the lifetime of the component.  While it's technically correct that memory is memory in our web application, it's best not to expose many surfaces for bad guys to poke at.  

## Example 1: The Basics without Redux

Try playing around with `example-1-reactjs-no-redux` application in the directory and notice the following aspects of this very simple project that serves its own data to itself:
1. The application has a top level component called `App` that contains the simple `fetch` for the data.
2. There is no redux, instead state is handled at two different levels throughout the components.
3. At the top level we establish our data and pass it down through the component tree.
4. The component `SvgSelector` is handling which index of SVG we are currently viewing.
5. The component `SvgButton` is invoking a callback to `SvgSelector` via a function prop.  While it's invoking the `onClick` event, it's reporting to its parent component the change.
Although this is a rudamentary example, we'll build on it more and eventually switch to redux.  

Question: Why does the project manage the SVG index at the `SvgSelector` level?  Why not just have state being managed by the `App` component?  Answer: the state should be managed at the first common parent component of the components that need to know about this state.  In this simple application, `SvgSelector` is the parent to `SvgContainer` and `SvgButton`.  The button component passes the change up to its parent's state which then binds it to the prop on `SvgContainer` which then rerenders and displays the SVG belonging to that index.

Question: Why not just compute the SVG and pass only that into `SvgContainer`?  Answer: What a great idea!  Unless there are other constraints, developers should err on the side of fewer props.  This doesn't mean we should reconstruct props into some catchall single prop object as props should be as granular as services and input (models in general) dictate.  There will be more on that later.  It's quite difficult to see ahead of time where your objects that you're connected to in Redux should be split so don't be afraid to refactor or review code with fresh eyes later after many changes have been added.  If you've written tests well enough, you should be able to do this without much fear of breaking something unknown.  

## Example 2: Enter Redux

After spending a small amount of time with Exmaple 1, take a look at Example 2.  It should be identical to Example 1 with one simple difference: state is managed by Redux.  Instead of a concept of "services", we are now using actions and reducers.  This is a very simple example that we will build upon.  If you want to know the basics of Redux, there are plenty of great materials out there but this document assumes the reader knows these core concepts already.  

Consider the following questions between 1 & 2:
1. Aside from how they're provided to the `render()`, What is the difference between `this.state.svgList` in `example-1-reactjs-no-redux/src/App.js` and `this.props.svgList` in `example-2-reactjs-with-redux/src/App.js`?
2. If you were to write tests for the logic that fetches and provides the data to our component, which project would be easier to accommodate these test?
3. What differences do you notice in each project's `App.test.js` file?  Why is this extra boilerplate needed in the second application?  Is it necessary?  

## Example 3: Mix in Lots of Data

Sometimes it's just a few SVGs you're flipping between.  But usually redux and state are designed to hold much more than that.  Most of the time you're going to feel like you're dealing with a novel sized JSON object.  So let's add a novel to the mix and see how it handles rendering it.  In `example-3-reactjs-ulysses`, there's a big JSON file (that may cause your computer to slow to a crawl if you open it) located at `example-3-reactjs-ulysses/public/ulysses.json`.  It's James Joyce's _Ulysses_ extracted into only words with some section, chapter and sentence structure baked into the JSON.  Before proceeding, read `scaffolding/ulysses.txt`.  Ha, just kidding.  Kinda.  It's a great book!

On my personal laptop this takes 6.5 seconds to load!  Should work for a great example project in the coming demonstrations.

## Example 4: Lots of Data the Hard Way

Did you notice how example 3 loaded the whole JSON structure and then digested it and displayed it using only three components?  That's the "correct" way to handle something like this -- for tons of reasons!  Among them the fact that if we wanted to markup all sentences (say left justify them) then we'd just have to fix it in one place.  Unfortunately it's not really the reality of sufficiently complex real world examples.  

So I'm going to do something bad for the sake of demonstration (did I mention you should ignore the scaffolding directory at all costs?).  You can see how we can achieve this rendering with just a handful of components in the third example but most real life large applications (like our repo) have hundreds or even thousands of composable components.  And that's the scenario where things get interesting.  

Please note that this causes an issue on some operating systems where there are too many open watches on the file system so for example in linux I had to run: `echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p` and then `sysctl --system`.  Also please note that from here on out there will be some waiting when your server starts up but we will reduce this exercise to just the first of the three sections of _Ulysses_.  

So the entire purpose of this section is to establish a baseline for many components.  If you `cd scaffoding/` in the project and run `yarn e4` it will generate the many components we need for exmaple 4.  After running `yarn start` you may have to wait several minutes befroe the service starts up but you will see the first section of _Ulysses_ displayed similar to the examples above.  We will use this rendering as a baseline since there's no redux involved and is totally just a lot of vanilla components.  

Once you've run the project, open the page in Google Chrome or a variant like Google Chrome.  Open developer tools (Option + &#8984; + i or in Linux Ctrl + Shift + i) and go to the performance tab.  Start to record and reload the page.  On my old core i5 laptop with lots of crap running, it takes a good 5 to 6.5 seconds for the render function alone to compute.  This is because there are tons of sub components rendering down below the main render function.  That topmost render function that you can easily see rendering for 5 seconds actually has a tree beneath it with each render function of each child component showing how much time it's taking.  For example I have zoomed in down to a leaf component `World.js` in the following screenshot:
![Image of World.js being rendered in performance tab](/images/01-example-04-worldjs-rendering.png)
As you can see in this instance it took 0.44 seconds for the client to complete rendering this particular instance of `World.js` so this performance tab is quite useful when trying to determine your biggest time goblins when a page or component is misbehaving.  

A typical completion of this page runs about 11 to 12 seconds for the scripting.

Now we're ready to delve into the two extremes of ReactJS and Redux.

## Example 5: Too Many Connected Components

In the first of the most ridiculous examples ever, we connect every word in the first section of Ulysses to redux.  In the scaffolding directory, run `yarn e5` and then in the example 5 directory run `yarn start`.  It may take several minutes to start up the server but when you finally get the page running, profile it in Chrome to see results similar to these:
![Image of total rendering in example 5](/images/02-example-05-total-rendering.png)
We can see here that it took over 17 seconds to finish all of the scripting on this page.  This is a good 5 to 6 more seconds than the example with no Redux above in example 4.  Before you come to the erroneous conclusion that this is not the way to do things.  This is an extreme example on a contrived project (rendering _Ulysses_?  also snobby) that isn't typical of what you do with React and Redux.  So let the pendulum swing the other way!

_pros_:
* It's simple to understand where your data is coming from.
* There are far less props to be passed down.
* When a parent component updates, it's easy to control for fewer rerenders on child components.
* When your redux state mutates, it's easier to restrict redux bindings from uncessarily updating components.

_cons_:
* Reuse is very difficult unless you are reusing _everything_ about a given component in the tree.
* If too many leaves are connected overhead is introduced (as demonstrated above).
* There are more connected components to debug if you're not sure which one has potential needless churning going on.
* Testing many connected components can be difficult and you will often have to export the component unconnected to aid in testing.
* It's harder to refactor large code bases away from this strategy than it is to refactor towards this strategy.

One thing to say about Example 5 is that the code is trim and it's incredibly easy to find that what you are connecting to (it's on the leaves).  

## Example 6: Drill, Baby, Drill

In the second of the most ridiculous examples ever, we connect only one -- the topmost -- component and shove all those props downward.  You can see this magical project by running `yarn e6` in the scaffolding directory and `yarn start` in the example 6 folder.  Again, it takes a while for this to start up but when it does, you can expect performance on the order below:
![Image of total rendering in example 6](/images/03-example-06-total-rendering.png)
In this particular run it took some 13 to 14 seconds to run all the scripting in this page.  Which is better than connecting everything but not as good (of course) as not delegating state management to redux.

:exclamation: So this is the best method for redux, right?  No.  Well, not necessarily.  You should be sensing the "everything's a double edged sword" vibe at this point.  And it is.  Checkout one of the chapter components, say `example-6-reactjs-redux-one-connected/src/book/SectionOne/ChapterOne/index.js` and notice that everything needs a prop for example `sentence={chapter.HerDoorWasOpenSheWantedToHearMyMusicSilentWithAweAndPity}` pushes the sentence down and everything needs to be pushed down.  This might not seem like a terrible thing but imagine a complex leaf that needs five or six dispatch functions from the great ancestor's `mapDispatchToProps` -- that's five or six props that need to be passed down the entire tree to that component!

_pros_:
* There's only one connected component so you know where your data's coming from.
* Reuse is incredibly simple at any point in the hierarchy of the component tree (after the root, it's all props) and overriding/extending is trivial.
* Testing "dumb" (unconnected) components is incredibly easy.
* Mounting entire trees and treating them like mini-applications in your tests is possible.
* If you reuse a lot of modals throughout your code, you can actually check their rendering instead of just a state bit flip.  

_cons_:
* A lot of property drilling down the tree.
* More prone to needless rerenders when the root component updates and erroneously tells all children to update when redux state changes.  
* There are a lot of function and callback function props passed down to simulate inter component communication that redux state can handle otherwise.

In the coming sections, we're going to analyze more and more the trade offs between these two strategies.  If you want the tl;dr on what the best methodology is and you're not going to consider any aspects of the project, it's usually best to start with example 6 and slowly concede to closer to example 5 when it makes the most sense.  But we'll get into why that's a good general strategy in the following sections.  

# Testing and Debugging Strategies

Coding the components is only one piece of the puzzle.  There's a lot more to software development than just writing a few classes.  The following sections aim to prepare you with proper testing strategies and procedures and help you understand how structuring your redux can affect your testing strategies.  

## Debugging Basics

TBD: Use example 3 to illustrate how to identify in chrome the different stages of how data gets from JSON to DOM.

## Example 7: Testing ReactJS components

Perhaps by now you've stumbled upon `renders without crashing` in the `App.test.js` and probably have been curious about the `Provider` and its `mockStore`.  The purpose of that code is to recreate a mock redux store and provide that state to the child component (in this case `App`).  This might seem confusing but for a connected component, these are necessary steps to provide the state that it expects to connect to in the redux store.  

For the purposes of "doing things right" to start with, we'll take Example 3 and add tests to it.  Our preferred way of writing tests is to make a `__test__` directory in the same directory as the components that are being tested.  You can also see that a `__fixtures__` directory has been added below that.  The purpose of fixtures is to store large data structures (as either JSON or JS files) in order to run tests.  In our case, the complexity is in the sentence data so we'll grab a tiny snippet from our payload (ulysses.json) but in reality, you're going to want to store meaningful and current examples of payloads throughout these directories that help you test all the possible oddities that can come from the service.  

Let's take a look at `example-7-basic-reactjs-testing/src/components/__test__/NovelComponent.test.js` where you can see a number of tests each doing different things.  

## Testing Pieces of Redux

# Structure

This section should contain rules that dictate where and how our code is placed in our tree.

## Container guidance

TBD: Discuss when something should be a container or a component and show good and bad examples of helpers for components.

# The Horrors

Horror shows happen naturally in code and part of being a good developer is identifying potential horror shows and implementing mitigation strategies.  The reality is that often things are double edged swords so you may find yourself selecting the lesser of two evils.  When dealing with horror shows, don't concentrate on who or when but concentrated on what and why.  Frequently they were deemed to be not as bad as the other possibilities.

## Prop Drilling

Prop drilling is when you open up component after component and it's towers of props all the way down the component tree.  A nice way of saying prop drilling is "very explicit prop declaration at every level".  Okay so maybe there's no great way to describe it.  It can lead to mouse wheel burnout as you try to scroll past props and generally the upper levels of the tree are endless props.  It can also cause git thrashing between people trying to manipulate the trunk components (though if they're kept simple and alphabatized, merges can be less painful).

## Everything is Connected

Everything is connected is the opposite extreme of prop drilling.  Basically all components are connected components and rely on your redux store being a very particular way.  Everything is connected is usually more concise than prop drilling but it can lead to difficult reuse even when unconnected component is exported (the store must be prepared, the same actions usually need to be used, etc).  It can also be more tedious to integration test these components -- especially when they contain modals.  If you're a functional developer you might claim all the components produce & rely on side effects.  

## Needless Rerenders

ReactJS is constantly checking if it needs to rerender a given component -- it's part of its raison d'etre.  And we've all been there where we've been misunderstanding what indication we're giving it to rerender.  This generally leads to slow code and a terrible user experience.  It can creep up on you, it compounds, as you build around your component and reuse it, the real warts can being to manifest.  Luckily there are lots of tools to get to the bottom of what's causing it to be a cycle goblin.

## Untestability

One of the key paradigms of keeping production code happy is separation of concerns.  Everything from your helper functions to your component's class member functions to your redux functionality should strive for a single purpose or at least be decomposed so that sufficient testing can be done.  Early choices made in your design can prohibit this so it bears stopping every now and then and asking yourself how you plan to test this.  
