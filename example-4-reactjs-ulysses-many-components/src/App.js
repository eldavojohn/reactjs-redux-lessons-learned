import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import SectionOne from './book/SectionOne';
// import SectionTwo from './book/SectionTwo';
// import SectionThree from './book/SectionThree';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to Ulysses Example 4</h1>
        </header>
        <p className="App-intro">
          {SectionOne ? <SectionOne /> : <div>Please run the "yarn e4" target in the scaffolding directory and restart this server</div>}
          {/* {SectionTwo && <SectionTwo />}
          {SectionThree && <SectionThree />} */}
        </p>
      </div>
    );
  }
}

export default App;
