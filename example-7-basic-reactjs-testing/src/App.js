import React, { Component } from 'react';
import { connect } from 'react-redux';

import logo from './logo.svg';
import './App.css';
import NovelComponent from './components/NovelComponent';
import { fetchNovel } from './redux/actions/novel';

export class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      error: null,
    };
  }
  async componentDidMount() {
    this.props.fetchNovel();
  }
  render = () => {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Example 7: Demonstration Testing App</h1>
        </header>
        <div className="App-intro">
          <NovelComponent novel={this.props.novel} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({ ...state });
const mapDispatchToProps = { fetchNovel };
const AppContainer = connect(mapStateToProps, mapDispatchToProps)(App);

export default AppContainer;
