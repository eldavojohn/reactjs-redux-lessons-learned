export const someSentences = {
  "MomentALittleTroubleAboutThoseWhiteCorpusclesSilenceAll": {
    "data": {
      "209": "Moment",
      "210": "A",
      "211": "Little",
      "212": "Trouble",
      "213": "About",
      "214": "Those",
      "215": "White",
      "216": "Corpuscles",
      "217": "Silence",
      "218": "All"
    }
  },
  "HePeeredSidewaysUpAndGaveALongSlowWhistleOfCallThenPaused": {
    "data": {
      "219": "He",
      "220": "Peered",
      "221": "Sideways",
      "222": "Up",
      "223": "And",
      "224": "Gave",
      "225": "A",
      "226": "Long",
      "227": "Slow",
      "228": "Whistle",
      "229": "Of",
      "230": "Call",
      "231": "Then",
      "232": "Paused"
    }
  },
  "AwhileInRaptAttentionHisEvenWhiteTeethGlisteningHereAndThere": {
    "data": {
      "233": "Awhile",
      "234": "In",
      "235": "Rapt",
      "236": "Attention",
      "237": "His",
      "238": "Even",
      "239": "White",
      "240": "Teeth",
      "241": "Glistening",
      "242": "Here",
      "243": "And",
      "244": "There"
    }
  },
  "WithGoldPointsChrysostomosTwoStrongShrillWhistlesAnswered": {
    "data": {
      "245": "With",
      "246": "Gold",
      "247": "Points",
      "248": "Chrysostomos",
      "249": "Two",
      "250": "Strong",
      "251": "Shrill",
      "252": "Whistles",
      "253": "Answered"
    }
  },
  "ThroughTheCalm": {
    "data": {
      "254": "Through",
      "255": "The",
      "256": "Calm"
    }
  },
  "ThanksOldChapHeCriedBrisklyThatWillDoNicelySwitchOff": {
    "data": {
      "257": "Thanks",
      "258": "Old",
      "259": "Chap",
      "260": "He",
      "261": "Cried",
      "262": "Briskly",
      "263": "That",
      "264": "Will",
      "265": "Do",
      "266": "Nicely",
      "267": "Switch",
      "268": "Off"
    }
  },
  "TheCurrentWillYou": {
    "data": {
      "269": "The",
      "270": "Current",
      "271": "Will",
      "272": "You"
    }
  },
  "HeSkippedOffTheGunrestAndLookedGravelyAtHisWatcherGathering": {
    "data": {
      "273": "He",
      "274": "Skipped",
      "275": "Off",
      "276": "The",
      "277": "Gunrest",
      "278": "And",
      "279": "Looked",
      "280": "Gravely",
      "281": "At",
      "282": "His",
      "283": "Watcher",
      "284": "Gathering"
    }
  }
};
