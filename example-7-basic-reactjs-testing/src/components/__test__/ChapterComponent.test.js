import React from 'react';
import { shallow, mount } from 'enzyme';

import ChapterComponent from '../ChapterComponent';

import { someSentences } from './__fixtures__/SentenceFixtures';

it('shallow renders without crashing', () => {
  const wrapper = shallow(<ChapterComponent title="just a simple title" chapter={someSentences}/>);

  expect(wrapper).toMatchSnapshot();
});

it('full mount renders without crashing', () => {
  const wrapper = mount(<ChapterComponent title="just a simple title" chapter={someSentences}/>);

  expect(wrapper).toMatchSnapshot();
});
