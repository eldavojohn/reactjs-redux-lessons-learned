import React from 'react';
import { shallow, mount } from 'enzyme';

import NovelComponent from '../NovelComponent';

import { someSentences } from './__fixtures__/SentenceFixtures';

const aNovel = {
  "section one": {
    "chapter one": someSentences,
    "chapter two": someSentences
  },
  "section two": {
    "chapter three": someSentences,
    "chapter four": someSentences
  }
}

it('shallow renders without crashing', () => {
  const wrapper = shallow(<NovelComponent title="just a simple title" novel={aNovel}/>);

  expect(wrapper).toMatchSnapshot();
});

it('full mount renders without crashing', () => {
  const wrapper = mount(<NovelComponent title="just a simple title" novel={aNovel}/>);

  expect(wrapper).toMatchSnapshot();
});
