import React from 'react';
import { shallow, mount } from 'enzyme';

import SectionComponent from '../SectionComponent';

import { someSentences } from './__fixtures__/SentenceFixtures';

const someChapters = {
  "chapter one": someSentences,
  "chapter two": someSentences
}

it('shallow renders without crashing', () => {
  const wrapper = shallow(<SectionComponent title="just a simple title" section={someChapters}/>);

  expect(wrapper).toMatchSnapshot();
});

it('full mount renders without crashing', () => {
  const wrapper = mount(<SectionComponent title="just a simple title" section={someChapters}/>);

  expect(wrapper).toMatchSnapshot();
});
