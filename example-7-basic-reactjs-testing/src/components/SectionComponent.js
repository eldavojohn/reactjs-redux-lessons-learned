import React, { Fragment } from 'react';

import ChapterComponent from './ChapterComponent';

const SectionComponent = (props) => {
  const {
    section = {},
    title = '',
  } = props;
  const chapters = Object.keys(section);
  const text = chapters.map(chapter => <ChapterComponent chapter={section[chapter]} title={chapter} key={chapter} />);
  return <Fragment><h1>{title}</h1>{text}</Fragment>;
}

export default SectionComponent;
