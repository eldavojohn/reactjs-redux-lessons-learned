import React from 'react';

import SectionComponent from './SectionComponent';

const NovelComponent = (props) => {
  const { novel = {} } = props;
  const sections = Object.keys(novel);
  return sections.map(section => <SectionComponent section={novel[section]} title={section} key={section} />);
}

export default NovelComponent;
