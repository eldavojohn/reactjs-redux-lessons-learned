import React, { Fragment } from 'react';

const ChapterComponent = (props) => {
    const {
      chapter = {},
      title = '',
    } = props;
    const sentences = Object.keys(chapter);
    const text = sentences.map(sentence => < div key = {
        Object.keys(chapter[sentence].data).join('')
      } > {
        Object.values(chapter[sentence].data).join(' ')
      } < /div>);
    return <Fragment><h2 data-test="chaptercomponent-title">{title}</h2>{text}</Fragment>;
    }

    export default ChapterComponent;
