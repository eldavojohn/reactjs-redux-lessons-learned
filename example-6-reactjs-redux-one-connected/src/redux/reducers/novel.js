import { combineReducers } from 'redux';

export const novel = (state = [], action) => {
  switch (action.type) {
    case 'FETCH_NOVEL':
      return action.payload;
    case 'CLEAR_NOVEL':
      return { };
    default:
      return state;
  }
};

export const reducers = combineReducers({ novel });
