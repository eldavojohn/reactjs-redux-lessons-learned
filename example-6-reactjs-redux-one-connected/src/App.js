import React, { Component } from 'react';
import { connect } from 'react-redux';
import idx from 'idx';

import logo from './logo.svg';
import './App.css';
import { fetchNovel } from './redux/actions/novel';

import SectionOne from './book/SectionOne';
// import SectionTwo from './book/SectionTwo';
// import SectionThree from './book/SectionThree';

class App extends Component {
  async componentDidMount() {
    this.props.fetchNovel();
  }
  render() {
    const section = idx(this.props, _ => _.novel.SectionOne);
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to Ulysses Example 6 Connected Root</h1>
        </header>
        <p className="App-intro">
          {SectionOne ? <SectionOne section={section} /> : <div>Please run the "yarn e6" target in the scaffolding directory and restart this server</div>}
          {/* {SectionTwo && <SectionTwo />}
          {SectionThree && <SectionThree />} */}
        </p>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({ ...state });
const mapDispatchToProps = { fetchNovel };
const AppContainer = connect(mapStateToProps, mapDispatchToProps)(App);

export default AppContainer;
