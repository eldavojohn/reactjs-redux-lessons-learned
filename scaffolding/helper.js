const {
  sep
} = require("path")

// this is throwaway code, not meant for human consumption!
// please do not read this!  By reading this file, you agree
// not to sue for loss of sleep, sanity or general wellbeing!

const repeat = n => {
  let result = ``
  for (var i = 0; i < n; i++) {
    result = `${result}${sep}..`
  }
  return result
}

const sepRegex = new RegExp(sep, 'g')

const pageShell = (imports, interior, hydratingPropName) => `import React, { Component, Fragment } from 'react'
${imports ? `
${imports}

` : ''}export default class extends Component {
  render() {
    ${hydratingPropName ? `const { ${hydratingPropName} = {} } = this.props;` : ''}
    return (<Fragment>
      ${interior}{' '}
    </Fragment>);
  }
}`

const connectedWordShell = (imports, interior, binding) => `import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux';
import idx from 'idx';
${imports ? `
${imports}

` : ''}class Word extends Component {
  render() {
    return (<Fragment>
      {this.props.word}{' '}
    </Fragment>);
  }
}

const mapStateToProps = state => ({
  word: idx(state, _ => _.novel.${binding})
})
const WordContainer = connect(mapStateToProps)(Word);

export default WordContainer;`

exports.sentencePage = (elements, obj, hydratingPropName) => {
  const { data = {} } = obj
  const words = [...new Set(Object.values(data))]
  const indices = Object.keys(data)
  return pageShell(
    words.map(word => `import ${word} from '../../../dictionary/${word[0]}/${word}'`).join('\n'),
    `${indices.map((el, i) => `<${data[el]} index={${el}} key={${i}} ${hydratingPropName ?
      `word={${hydratingPropName} && ${hydratingPropName}.data && ${hydratingPropName}.data['${el}']}` : ''} />`).join('\n      ')}\n      <br />`,
    hydratingPropName
  )
}

exports.parentPage = (elements, obj, hydratingPropName) => {
  let submodule
  if(hydratingPropName === 'section') {
    submodule = 'chapter'
  }
  if(hydratingPropName === 'chapter') {
    submodule = 'sentence'
  }
  return pageShell(
    elements.map(el => `import ${el} from './${el}'`).join('\n'),
    elements.map((el, i) => `<${el} key={${i}} ${hydratingPropName ? `${submodule}={${hydratingPropName}.${el}}` : ''} />`).join('\n      '),
    hydratingPropName
  )
}

exports.wordPage = (word, binding) => binding ?
  connectedWordShell('', `<span index={this.props.index}>${word}</span>`, binding) :
  pageShell('', `<span index={this.props.index}>${word}</span>`)

exports.simpleWordPage = word => pageShell('', `<span index={this.props.index}>{this.props.word}</span>`)
