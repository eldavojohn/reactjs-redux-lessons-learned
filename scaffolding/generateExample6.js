const {
  jsonToFsStructure,
  jsonToFsWithLeafFunction,
  jsonToFsWithNonLeafFunction,
  jsonToFsWithFunction
} = require("json-to-fs-structure")
const fs = require("fs")
const {
  sep
} = require("path")

// this is throwaway code, not meant for human consumption!
// please do not read this!  By reading this file, you agree
// not to sue for loss of sleep, sanity or general wellbeing!

const noop = () => {}

const readProjectPath = `..${sep}example-3-reactjs-ulysses`
const nodePublicPath = `${readProjectPath}${sep}public`
const ontologyFile = fs.readFileSync(`${nodePublicPath}${sep}ulysses.json`)
const ontology = JSON.parse(ontologyFile)
const { parentPage, sentencePage, simpleWordPage } = require('./helper')
const topProjectPath = `..${sep}example-6-reactjs-redux-one-connected`
fs.writeFile(`${topProjectPath}${sep}public${sep}ulysses.json`, JSON.stringify(ontology), noop)
const srcPath = `${topProjectPath}${sep}src`
const projectPath = `${srcPath}${sep}book`
const dictionaryPath = `${projectPath}${sep}dictionary`
const endpoints = []

try {
  fs.mkdirSync(topProjectPath)
} catch (err) {}
try {
  fs.mkdirSync(srcPath)
} catch (err) {}
try {
  fs.mkdirSync(projectPath)
} catch (err) {}
try {
  fs.mkdirSync(dictionaryPath)
} catch (err) {}
try {
  "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split('').forEach(letter =>
    fs.mkdirSync(`${dictionaryPath}${sep}${letter}`)
  )
} catch (err) {}

jsonToFsWithNonLeafFunction({
  jsonObject: ontology,
  nonLeafProcedure: (filePath, acc, obj = {}, propertyValue) => {
    const sepRegex = new RegExp(sep, 'g')
    const elements = Object.keys(obj)
    let writeFileTemplate
    let hydratingPropName
    const depth = filePath.match(sepRegex).length - 4
    if(depth === 0) {
      hydratingPropName = 'section'
      writeFileTemplate = parentPage
    }
    if(depth === 1) {
      hydratingPropName = 'chapter'
      writeFileTemplate = parentPage
    }
    if(depth === 2) {
      hydratingPropName = 'sentence'
      writeFileTemplate = sentencePage
    }
    if(writeFileTemplate) {
      fs.writeFile(`${filePath}${sep}index.js`, writeFileTemplate(elements, obj, hydratingPropName), noop)
    }
    return acc
  },
  filePath: projectPath,
  context: { 'dictionary': []},
  stopWord: ['data', 'SectionTwo', 'SectionThree'] // for now lets just do section one
})

const getKeyByValue = (object, value) => {
  return Object.keys(object).find(key => object[key] === value);
};

jsonToFsWithLeafFunction({
  jsonObject: ontology,
  leafProcedure: (filePath, acc, obj) => {
    const words = Object.values(obj)
    const { dictionary } = acc
    words.forEach(word => {
      if(dictionary.indexOf(word) === -1) {
        const firstLetter = word[0]
        fs.writeFile(
          `${dictionaryPath}${sep}${firstLetter}${sep}${word}.js`,
          simpleWordPage(word), noop)
        acc.dictionary.push(word)
      }
    })
    return acc
  },
  context: { 'dictionary': []},
  filePath: projectPath,
  stopWord: ['data', 'SectionTwo', 'SectionThree'] // for now lets just do section one
})
