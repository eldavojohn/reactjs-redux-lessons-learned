const {
  jsonToFsStructure,
  jsonToFsWithLeafFunction,
  jsonToFsWithNonLeafFunction,
  jsonToFsWithFunction
} = require("json-to-fs-structure")
const fs = require("fs")
const {
  sep
} = require("path")

// this is throwaway code, not meant for human consumption!
// please do not read this!  By reading this file, you agree
// not to sue for loss of sleep, sanity or general wellbeing!

const readProjectPath = `..${sep}example-3-reactjs-ulysses`
const nodePublicPath = `${readProjectPath}${sep}public`
const ontologyFile = fs.readFileSync(`${nodePublicPath}${sep}ulysses.json`)
const ontology = JSON.parse(ontologyFile)
const { parentPage, sentencePage, wordPage } = require('./helper')
const topProjectPath = `..${sep}example-4-reactjs-ulysses-many-components`
const srcPath = `${topProjectPath}${sep}src`
const projectPath = `${srcPath}${sep}book`
const dictionaryPath = `${projectPath}${sep}dictionary`
const endpoints = []

const noop = () => {}

try {
  fs.mkdirSync(topProjectPath)
} catch (err) {}
try {
  fs.mkdirSync(srcPath)
} catch (err) {}
try {
  fs.mkdirSync(projectPath)
} catch (err) {}
try {
  fs.mkdirSync(dictionaryPath)
} catch (err) {}
try {
  "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split('').forEach(letter =>
    fs.mkdirSync(`${dictionaryPath}${sep}${letter}`)
  )
} catch (err) {}

jsonToFsWithNonLeafFunction({
  jsonObject: ontology,
  nonLeafProcedure: (filePath, acc, obj = {}, propertyValue) => {
    const sepRegex = new RegExp(sep, 'g')
    const elements = Object.keys(obj)
    let writeFileTemplate
    const depth = filePath.match(sepRegex).length - 4
    if(depth === 0 || depth === 1) {
      writeFileTemplate = parentPage
    }
    if(depth === 2) {
      writeFileTemplate = sentencePage
    }
    if(writeFileTemplate) {
      fs.writeFile(`${filePath}${sep}index.js`, writeFileTemplate(elements, obj), noop)
    }
    return acc
  },
  filePath: projectPath,
  context: { 'dictionary': []},
  stopWord: ['data', 'SectionTwo', 'SectionThree'] // for now lets just do section one
})

jsonToFsWithLeafFunction({
  jsonObject: ontology,
  leafProcedure: (filePath, acc, obj) => {
    const words = Object.values(obj)
    const { dictionary } = acc
    words.forEach(word => {
      if(dictionary.indexOf(word) === -1) {
        const firstLetter = word[0]
        fs.writeFile(
          `${dictionaryPath}${sep}${firstLetter}${sep}${word}.js`,
          wordPage(word), noop)
        acc.dictionary.push(word)
      }
    })
    return acc
  },
  context: { 'dictionary': []},
  filePath: projectPath,
  stopWord: ['data', 'SectionTwo', 'SectionThree'] // for now lets just do section one
})
