const fs = require("fs");

// this is throwaway code, not meant for human consumption!
// please do not read this!  By reading this file, you agree
// not to sue for loss of sleep, sanity or general wellbeing!

const {
  sep
} = require("path");

const handleError = err => { if(err) { console.log(err); } };

const naturalNumbers = ['One','Two','Three','Four', 'Five','Six','Seven','Eight','Nine','Ten','Eleven','Twelve','Thirteen','Fourteen','Fifteen','Sixteen','Seventeen','Eighteen'];
const nonAlphaSpace = new RegExp(/[^a-zA-Z\s]/, 'g');
const projectPath = '../example-3-reactjs-ulysses';
const nodePublicPath = `${projectPath}${sep}public`;

try {
  fs.mkdirSync(projectPath);
} catch (err) {}

try {
  fs.mkdirSync(nodePublicPath);
} catch (err) {}

const romanNumerals = {
  'I': 'One',
  'II': 'Two',
  'III': 'Three'
};

const processChapters = chapterString => {
  const lines = chapterString.split('\n');
  const chapterLine = {};
  let index = 0;
  lines.forEach(line => {
    const cleanLine = line.replace(nonAlphaSpace, '');
    const cleanArray = cleanLine.split(/\s/g).map(word => {
      const trimmed = word.trim();
      if(trimmed && trimmed.length > 0) {
        const lower = trimmed.toLowerCase();
        return lower.charAt(0).toUpperCase() + lower.substr(1);
      }
    });
    if(cleanArray.join('')) {
      chapterLine[cleanArray.join('')] = cleanArray.reduce((acc, rawWord) => {
        let word = rawWord;
        if(rawWord === "Component") {
          word = "ComponentComponent"
        } else if(rawWord === "Fragment") {
          word = "FragmentComponent"
        }
        acc['data'][++index] = word;
        return acc;
      }, { data: {} });
    }
  });
  return chapterLine;
};

const processSection = sectionString => {
  const integerSectionRegex = /\[ ([0-9]+) \]/;
  const sectionRegexp = new RegExp(integerSectionRegex, 'g');
  const splits = sectionString.split(/\[ [0-9]+ \]/);
  let matchedChapters = sectionRegexp.exec(sectionString);
  let sectionIndex = 0;
  const chapter = {};
  while (matchedChapters != null) {
    chapter['Chapter' + naturalNumbers[matchedChapters[1] - 1]] = processChapters(splits[++sectionIndex])
    matchedChapters = sectionRegexp.exec(sectionString);
  }
  return chapter;
};

fs.readFile('ulysses.txt', (err, data) => {
  handleError(err);
  const ulysses = {};
  const ulyssesText = data.toString();
  const romanNumeralSectionRegex = /\— ([MDCLXVI]+) \—/;
  const sectionRegexp = new RegExp(romanNumeralSectionRegex, 'g');
  const splits = ulyssesText.split(/\— [MDCLXVI]+ \—/);
  let matchedSections = sectionRegexp.exec(ulyssesText);
  let sectionIndex = 0;
  while (matchedSections != null) {
    ulysses['Section' + romanNumerals[matchedSections[1]]] = processSection(splits[++sectionIndex])
    matchedSections = sectionRegexp.exec(ulyssesText);
  }
  fs.writeFile(`${nodePublicPath}${sep}ulysses.json`, JSON.stringify(ulysses), (err, data) => {
    handleError(err);
  });
});
