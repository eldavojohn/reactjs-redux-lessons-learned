const SVG_API = 'data.json';

async function fetchSvgs(successHandler, errorHandler) {
  try {
    const data = await fetch(SVG_API)
      .then(response => response.json());
    successHandler(data.svgs);
  } catch (error) {
    errorHandler(error);
  }
}

export default { fetchSvgs };
