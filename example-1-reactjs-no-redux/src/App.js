import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import SvgSelector from './components/SvgSelector';
import SvgService from './services/SvgService';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      svgList: [],
      isLoading: false,
      error: null,
    };
  }
  componentDidMount = () => {
    this.setState({ isLoading: true });
    SvgService.fetchSvgs(this.setSvgSuccess, this.setServiceError);
  }
  setSvgSuccess = svgList => {
    this.setState({
      svgList: svgList,
      isLoading: false
    });
  }
  setServiceError = error => {
    this.setState({
      error,
      isLoading: false
    });
  }
  render = () => {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Demonstration App</h1>
        </header>
        <div className="App-intro">
          <SvgSelector svgs={this.state.svgList} />
        </div>
      </div>
    );
  }
}

export default App;
