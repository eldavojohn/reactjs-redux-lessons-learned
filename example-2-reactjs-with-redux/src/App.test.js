import React from 'react';
import ReactDOM from 'react-dom';
import configureStore from 'redux-mock-store';
import { applyMiddleware } from 'redux';
import { Provider } from 'react-redux'
import thunk from 'redux-thunk';

import { App } from './App';

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

it('renders without crashing', () => {
  const store = mockStore({svgList: []});
  const div = document.createElement('div');
  ReactDOM.render(<Provider store={store}><App svgList={[]} fetchSvgs={jest.fn()} /></Provider>, div);
  ReactDOM.unmountComponentAtNode(div);
});
