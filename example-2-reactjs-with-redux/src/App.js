import React, { Component } from 'react';
import { connect } from 'react-redux';

import logo from './logo.svg';
import './App.css';
import SvgSelector from './components/SvgSelector';
import { fetchSvgs } from './redux/actions/svgs';

export class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      error: null,
    };
  }
  async componentDidMount() {
    this.props.fetchSvgs();
  }
  render = () => {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Demonstration App</h1>
        </header>
        <div className="App-intro">
          <SvgSelector svgs={this.props.svgList} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({ ...state });
const mapDispatchToProps = { fetchSvgs };
const AppContainer = connect(mapStateToProps, mapDispatchToProps)(App);

export default AppContainer;
