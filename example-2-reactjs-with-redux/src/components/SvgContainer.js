import React from 'react';

const SvgContainer = (props) => {
  return <span dangerouslySetInnerHTML={{__html: props.svgs[props.svgIndex]}} />;
}

export default SvgContainer;
