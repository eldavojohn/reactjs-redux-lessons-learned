import React, { Component } from 'react';
import SvgContainer from './SvgContainer';
import SvgButton from './SvgButton';

class SvgSelector extends Component {
  constructor(props) {
    super(props);

    this.state = {
      svgIndex: 0
    };
  }
  toggleSvg = () => {
    this.setState((prevState, props) => {
      return {svgIndex: (prevState.svgIndex + 1) % props.svgs.length};
    });
  }
  render = () => {
    const { svgs } = this.props;
    const { svgIndex } = this.state;
    return (
      <div>
        <br />
        <SvgContainer svgs={svgs} svgIndex={svgIndex} />
        <br />
        <br />
        <SvgButton toggleSvg={this.toggleSvg} />
      </div>
    );
  }
}

export default SvgSelector;
