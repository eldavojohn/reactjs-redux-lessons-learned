import React from 'react';

const SvgButton = (props) =>
  (<button onClick={props.toggleSvg}>
    next svg
  </button>);

export default SvgButton;
