import { combineReducers } from 'redux';

export const svgList = (state = [], action) => {
  switch (action.type) {
    case 'ADD_SVGS':
      return action.payload.svgs;
    case 'CLEAR_SVGS':
      return [];
    default:
      return state;
  }
};

export const reducers = combineReducers({ svgList });
