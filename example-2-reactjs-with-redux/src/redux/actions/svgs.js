const SVG_API = 'data.json';

export const addSvgs = payload => ({
  type: 'ADD_SVGS',
  payload,
});

export const clearSvgs = () => ({ type: 'CLEAR_SVGS' });

export const fetchSvgs = () => async dispatch => {
  try {
    const response = await fetch(SVG_API)
    const responseBody = await response.json();
    dispatch(addSvgs(responseBody));
  } catch (error) {
    console.error(error);
    dispatch(clearSvgs());
  }
}
